cmake_minimum_required(VERSION 2.8.0)
set(CMAKE_CXX_FLAGS "-std=c++11")
project("LittleTales")

file(GLOB demo_SRC
    "main.cpp"
    "./src/*.cpp"
    "./src/base/*.cpp"
    "./src/game/*.cpp"
    "./src/utils/*.cpp"
)

add_executable("LittleTales" ${demo_SRC})
find_library(GOMP_LIBRARY gomp HINTS ../gomp/)
find_library(SDL2_LIBRARY SDL2)
find_library(ROSQUI_LIB Rosquillera)
target_include_directories(LittleTales PUBLIC ./src/)
target_link_libraries(LittleTales PUBLIC ${ROSQUI_LIB})
target_link_libraries(
  LittleTales
  PUBLIC
  ${SDL2_LIBRARY}
  SDL2main
  SDL2_image
  SDL2_mixer
  SDL2_ttf
  ${GOMP_LIBRARY}
  chipmunk
)
