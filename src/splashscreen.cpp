#include "splashscreen.h"

#include <RosquilleraReforged/rf_primitive.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_soundmanager.h>

#include "mainprocess.h"

void SplashScreen::Start()
{
  RF_Engine::instance->Clock.setFixedCTime();

  RF_Layer::Start();
  setWindow(RF_Engine::getWindow(RF_Engine::MainWindow()));
  RF_Primitive::clearSurface(graph, 0xFFFFFF);

  RF_AssetManager::LoadAssetPackage("resources/splashscreen");

  srand(RF_Engine::instance->Clock.fixedCTime());
  logo = RF_Engine::getTask<SS_Logo>(RF_Engine::newTask<SS_Logo>(id));

  w = -320 + graph->w * 0.5;
  h = -240 + graph->h * 0.5;
}

void SplashScreen::Update()
{
  deltaCont += RF_Engine::instance->Clock.deltaTime;

  if(part == 0)
  {
    logo->transform.position.x = -1000;
    logo->graph = RF_AssetManager::Get<RF_Gfx2D>("splashscreen", "logo");
    step = 0;
  }
  else if(part == 1)
  {
    part1();
  }
  else if(part == 2)
  {
    /*RF_Primitive::clearSurface(graph, 0xffffff);*/
    logo->transform.position.x = -1000;
    logo->graph = RF_AssetManager::Get<RF_Gfx2D>("splashscreen", "banner");
    step = 0;
    deltaCont = 3.0;
  }
  else if(part == 3)
  {
    part1();
  }
  else
  {
    logo->transform.position.x = -1000;
    partend();
  }

  switch(pulso)
  {
    case 0:
      if(RF_Engine::instance->Clock.fixedCTime() >= 3004)
      {
        beat();
      }
      break;

    case 1:
      if(RF_Engine::instance->Clock.fixedCTime() >= 9571)
      {
        beat();
      }
      break;

    case 2:
      if(RF_Engine::instance->Clock.fixedCTime() >= 11082)
      {
        beat();
      }
      break;

    case 3:
      if(RF_Engine::instance->Clock.fixedCTime() > 17892)
      {
        beat();
      }
      break;
  }
}

void SplashScreen::beat()
{
  pulso++;
  part++;
}

void SplashScreen::part1()
{
  step+=(RF_Engine::instance->Clock.deltaTime*200);

  int stp = step;
  if(delay <= stp && step < delay+185){stp = delay;}
  else if(step >= delay+185){stp = delay - (step - (delay+185));}

  logo->transform.position.x = w + cos(RF_Engine::instance->Clock.fixedCTime() * 0.01) * (delay-stp)*1.25;
  logo->transform.position.y = h + sin(RF_Engine::instance->Clock.fixedCTime() * 0.01) * (delay-stp);
}
void SplashScreen::partend()
{
  if(RF_Engine::instance->Clock.fixedCTime() > 19000)
  {
    part++;
    //RF_Primitive::clearSurface(graph, 0x000000);
    RF_AssetManager::UnloadAssetPackage("splashscreen");
    MainProcess::instance->NextScene();
  }
}
