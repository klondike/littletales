#include "mainprocess.h"

#include "execontrol.h"
#include "splashscreen.h"
#include "mainmenu.h"
//#include "game/level.h"
#include "utils/parser.h"
#include "utils/locale.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>

int startingScene = _SPLASHSCREEN;
bool windowed = false;
bool debug = false;

MainProcess* MainProcess::instance;

void MainProcess::NextScene()
{
  ChangeScene(actualScene + 1);
}
void MainProcess::ChangeScene(unsigned int scene)
{
  if(scene > _FOO_SCENE || scene == actualScene){return;}
  RF_Engine::sendSignal(stateMachine, S_KILL);

  switch(scene)
  {
    case _SPLASHSCREEN:
      stateMachine = RF_Engine::newTask<SplashScreen>(id);
      break;
    case _MAIN_MENU:
      RF_Engine::Fps(120);
      stateMachine = RF_Engine::newTask<MainMenu>(id);
      break;
    /*case _GAME:
      stateMachine = RF_Engine::newTask<Level>(id);
      break;*/
    case _FOO_SCENE:
      RF_Engine::Status() = false;
      break;
  }

  RF_Engine::Debug("[SCENE ENTERING] " + stateMachine);
  actualScene = scene;
}

void MainProcess::Start()
{
  Parser parser("options.json");

  /*DEBUG*/
    RF_Engine::Debug(debug);

  /*EXECONTROL*/
    RF_Engine::newTask<ExeControl>(id);

  /*BASE DE DATOS*/

  /*CARGA DE RECURSOS*/

  /*CARGA DE IDIOMA*/
    string tmp_lang;
    Parser::json_string(parser.scenejs["lang"], tmp_lang);
    Locale::setLang(tmp_lang);
    RF_Engine::Debug("1 Idioma: " + Locale::getLang());

  /*RESOLUCIÓN*/
    Uint32 windowFlag = (windowed) ? SDL_WINDOW_OPENGL : SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN;
    window = RF_Engine::addWindow("Little Tales", 1280, 720, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowFlag);
    RF_Engine::MainWindow(window);

  ChangeScene(startingScene);
}
