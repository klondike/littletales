#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_layer.h>

class SS_Logo : public RF_Process
{
  public:
    SS_Logo():RF_Process("Logo"){}
    virtual ~SS_Logo(){}
};

class SplashScreen : public RF_Layer
{
    public:
        SplashScreen():RF_Layer("SplashScreen"){}
        virtual ~SplashScreen(){}

        virtual void Start();
        virtual void Update();

    private:
        SDL_Surface* bgImg;

        float deltaCont = 0.0f;
        float step;
        int pulso = 0;
        int w,h;
        int delay = 400;

        int part = 0;
        SS_Logo *logo;

        void part1();
        void beat();
        void partend();
};

#endif //SPLASHSCREEN_H
