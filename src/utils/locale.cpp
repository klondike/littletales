#include <string>
#include "locale.h"
using namespace std;

namespace Locale
{
  static string lang = "";

  void setLang(string language)
  {
    lang = language;
  }
  
  string getLang()
  {
    return lang;
  }

  string getText(string key)
  {
    if(lang == "")
    {
      setLang("spanish");
    }

    Parser parser("locale/" + lang + ".json");
    string tmp = "";
    Parser::json_string(parser.scenejs[key], tmp);
    return tmp;
  }
}
