#ifndef PARSER_H
#define PARSER_H

#include "json.h"

#include <RosquilleraReforged/rf_engine.h>
#include <string>
using namespace std;

class Parser
{
  public:
    Parser(string file);
    virtual ~Parser();

    static bool json_string(Json::Value & o, string & res);
    static bool json_float(Json::Value & o, float &res);
    static bool json_int(Json::Value & o, int & res);
    static bool json_bool(Json::Value & o, bool & res);
    static bool json_vector2float(Json::Value & o, Vector2<float> &res);
    static bool json_SDLColor(Json::Value & o, SDL_Color &res);

  	Json::Value scenejs;
};

#endif //PARSER_H
