#ifndef LOCALE_H
#define LOCALE_H

#include <string>
using namespace std;

namespace Locale
{
  extern void setLang(std::string language);
  
  extern std::string getLang();

  extern std::string getText(std::string key);
}

#endif //LOCALE_H
