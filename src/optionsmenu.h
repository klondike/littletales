#ifndef OPTIONSMENU_H
#define OPTIONSMENU_H

#include <RosquilleraReforged/rf_process.h>

class OptionsMenu : public RF_Process
{
  public:
    OptionsMenu():RF_Process("OptionsMenu"){}
    virtual ~OptionsMenu();

    virtual void Start(){}
    virtual void Update(){}

    void setGUI(){}
    void setJOY(){}

  private:
    void hideJOY(){}
    void hideGUI(){}

  private:
    bool howIs = false; //false = GUI, true = JOY

    string package = "";
    string font = "";
    string background = "";
    string music = "";
};

#endif //OPTIONSMENU_H
