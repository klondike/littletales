#include "mainmenu.h"
#include "mainprocess.h"
#include "gui.h"
#include "utils/parser.h"
#include "utils/locale.h"

#include <RosquilleraReforged/rf_soundmanager.h>
#include <RosquilleraReforged/rf_taskmanager.h>

MainMenu::~MainMenu()
{
}

void MainMenu_StartButton(int foo)
{
  MainProcess::instance->ChangeScene(_GAME);
}
void MainMenu_OptionsMenu(int foo)
{
  MainProcess::instance->ChangeScene(_OPTIONS_MENU);
}
void MainMenu_ExitButton(int foo)
{
  RF_Engine::Status() = false;
}

void MainMenu_Cursor::SetGfx(string package, string gfx)
{
  graph = RF_AssetManager::Get<RF_Gfx2D>(package, gfx);
}
void MainMenu_Cursor::SetPosition(Vector2<float> pos)
{
  transform.position = pos;
}

Vector2<float> MainMenu_Cursor::GetPosition()
{
  return transform.position;
}

void MainMenu::Start()
{
  RF_Engine::Debug("2 Idioma: " + Locale::getLang());
  bool errores = false;

  Parser parser("scenes/mainmenu.json");

  zLayer = 1001;

  errores |= !Parser::json_string(parser.scenejs["package"], package);
  errores |= !Parser::json_string(parser.scenejs["font"], font);
  errores |= !Parser::json_string(parser.scenejs["background"], background);
  errores |= !Parser::json_string(parser.scenejs["music"], music);
  errores |= !Parser::json_vector2float(parser.scenejs["options"]["start_text"]["position"], start_text_position);
  errores |= !Parser::json_SDLColor(parser.scenejs["options"]["start_text"]["color"], start_text_color);
  errores |= !Parser::json_vector2float(parser.scenejs["options"]["options_text"]["position"], options_text_position);
  errores |= !Parser::json_SDLColor(parser.scenejs["options"]["options_text"]["color"], options_text_color);
  errores |= !Parser::json_vector2float(parser.scenejs["options"]["exit_text"]["position"], exit_text_position);
  errores |= !Parser::json_SDLColor(parser.scenejs["options"]["exit_text"]["color"], exit_text_color);

  initGUI(package, font, id);
  RF_Engine::getTask<Mouse>(gui_mouse)->zLayer = 9999;

  if(background != "")
  {
    graph = RF_AssetManager::Get<RF_Gfx2D>(package, background);
  }
  if(music != "")
  {
    //RF_SoundManager::changeMusic(package, music);
  }

  RF_TextManager::Font = RF_AssetManager::Get<RF_Font>(gui_package, gui_font);
  options.push_back(RF_TextManager::Write<float>(Locale::getText("mainmenu_start"), start_text_color, start_text_position));
  options.push_back(RF_TextManager::Write<float>(Locale::getText("mainmenu_options"), options_text_color, options_text_position));
  options.push_back(RF_TextManager::Write<float>(Locale::getText("mainmenu_exit"), exit_text_color, exit_text_position));

  cursor = RF_Engine::getTask<MainMenu_Cursor>(RF_Engine::newTask<MainMenu_Cursor>(id));
  cursor->SetGfx(package, "cursor");
  //cursor->SetPosition(RF_Engine::getTask(options[opc])->transform.position);
}

void MainMenu::Update()
{
  if(RF_Input::key[_s])
  {
    if(!keyPressed)
    {
      keyPressed = true;
      opc++;
      if(opc >= options.size()){opc = 0;}
    }
  }
  else if(RF_Input::key[_w])
  {
    if(!keyPressed)
    {
      keyPressed = true;
      opc--;
      if(opc < 0){opc = options.size()-1;}
    }
  }
  else
  {
    keyPressed = false;
  }

  if(cursor->GetPosition() != RF_Engine::getTask(options[opc])->transform.position)
  {
    Vector2<float> tmpPos = RF_Engine::getTask(options[opc])->transform.position;
    tmpPos.y += 25;
    tmpPos.x += RF_Engine::getTask(options[opc])->graph->w;
    cursor->SetPosition(tmpPos);
  }
}
