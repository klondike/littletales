#ifndef MAINMENU_H
#define MAINMENU_H

#include <RosquilleraReforged/rf_process.h>

class MainMenu_Cursor : public RF_Process
{
  public:
    MainMenu_Cursor():RF_Process("MainMenu_Cursor"){}
    virtual ~MainMenu_Cursor(){}

    void SetGfx(string package, string gfx);
    void SetPosition(Vector2<float> pos);
    Vector2<float> GetPosition();
};

class MainMenu : public RF_Process
{
  public:
    MainMenu():RF_Process("MainMenu"){}
    virtual ~MainMenu();

    virtual void Start();
    virtual void Update();

  private:
    string text = "", text2 = "";
    MainMenu_Cursor *cursor;
    int opc = 0;

    string package = "";
    string font = "";
    string background = "";
    string music = "";


    Vector2<float> start_text_position, options_text_position, exit_text_position;
    vector<string> options;
    SDL_Color start_text_color, options_text_color, exit_text_color;

    bool keyPressed = false;
};

#endif //MAINMENU_H
