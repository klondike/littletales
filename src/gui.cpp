#include "gui.h"

#include <RosquilleraReforged/rf_structs.h>
#include <RosquilleraReforged/rf_collision.h>

string gui_package = "";
string gui_font = "";
string gui_mouse = "";
void initGUI(string path, string font, string display)
{
  RF_Engine::Debug("Iniciando GUI");
  RF_AssetManager::LoadAssetPackage("resources/"+path);
  gui_package = path;
  gui_font = font;

  gui_mouse = RF_Engine::newTask<Mouse>(display);
  RF_Engine::getTask<Mouse>(gui_mouse)->Configure(gui_package, "mouse");
}
void stopGUI(string display)
{
  RF_Engine::Debug("Deteniendo GUI");

  RF_Engine::sendSignal(display, S_KILL_CHILD);
  RF_AssetManager::UnloadAssetPackage(gui_package);

  gui_package = "";
  gui_font = "";
}

void Button::Update()
{
  if(graph == nullptr){return;}

  if(Mouse::instance->collision(this))
  {
    if(!Mouse::instance->OnClick)
    {
      if(state != 1)
      {
        if(state == 2)
        {
          action(0);
        }
        graph = RF_AssetManager::Get<RF_Gfx2D>(package, gfx+"_hover");
        state = 1;
      }
    }
    else
    {
      if(state != 2)
      {
        graph = RF_AssetManager::Get<RF_Gfx2D>(package, gfx + "_click");
        state = 2;
      }
    }
  }
  else
  {
    if(state != 0)
    {
      graph = RF_AssetManager::Get<RF_Gfx2D>(package, gfx);
      state = 0;
    }
  }
}

Mouse* Mouse::instance;

void Mouse::Start()
{
    zLayer = -100;
}

void Mouse::Update()
{
    transform.position.x = RF_Input::mouse.position.x;
    transform.position.y = RF_Input::mouse.position.y;

    OnClick = RF_Input::mouse.Click[_leftbutton];
}
bool Mouse::collision(RF_Process* p)
{
    Vector2<int> ppos1, ppos2, pscal1, pscal2;
    ppos1.x = p->transform.position.x; ppos1.y = p->transform.position.y;
    ppos2.x = transform.position.x; ppos2.y = transform.position.y;
    pscal2.x = 1; pscal2.y = 1;

    SDL_Rect tmp = p->getDimensions();
    pscal1 = Vector2<int>(tmp.w, tmp.h);
    return rectCollision(ppos1,pscal1,ppos2,pscal2);
    //return checkCollision(this, p);
}
